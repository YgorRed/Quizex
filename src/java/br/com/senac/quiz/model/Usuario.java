
package br.com.senac.quiz.model;


public class Usuario {
    private int id ; 
    private String nome ; 
    private String senha; 
    private String apelido ; 

    public Usuario() {
    }

    public Usuario(int id, String nome, String senha, String apelido) {
        this.id = id;
        this.nome = nome;
        this.senha = senha;
        this.apelido = apelido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    @Override
    public String toString() {
        return id + " - " + nome + " - " + senha + " - " + apelido ;
    }
    
    
    
    
    
    
    
    
}
