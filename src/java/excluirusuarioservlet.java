
import br.com.senac.quiz.dao.UsuarioDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns ="/Excluir")
public class excluirusuarioservlet extends HttpServlet {

    private UsuarioDAO dao = new UsuarioDAO();
    @Override
    protected void doGet(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {
        
        int id = Integer.parseInt(requisicao.getParameter("id"));
        
        PrintWriter saida = resposta.getWriter();
         
        
        saida.print("<html>");
        saida.print("<body>"); 
        saida.print("Excluido!"); 
        saida.print("<html>"); 
        saida.print("<body>"); 
        
       dao.delete(id);
    }
    
    
    
}
